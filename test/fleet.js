// Load dependency modules.
const assert = require('assert');
const fleet = require('../fleet');

describe('Fleet module', () => {
  describe('#getNumberOfEngineers()', () => {
    it('should return expected values', () => {
      assert.strictEqual(fleet.getNumberOfEngineers([15, 10], 12, 5), 3);
      assert.strictEqual(fleet.getNumberOfEngineers([11, 15, 13], 9, 5), 7);
      assert.strictEqual(fleet.getNumberOfEngineers([5, 7, 11], 4, 2), 11);
      assert.strictEqual(fleet.getNumberOfEngineers([5, 7, 11], 4, 3), 7);
      assert.strictEqual(fleet.getNumberOfEngineers([15, 4, 5], 8, 7), 3);
      assert.strictEqual(fleet.getNumberOfEngineers([16, 8, 6], 13, 7), 4);
      assert.strictEqual(fleet.getNumberOfEngineers([17, 15, 1], 1, 3), 11);
      assert.strictEqual(fleet.getNumberOfEngineers([5, 16, 9], 20, 9), 2);
      assert.strictEqual(fleet.getNumberOfEngineers([15, 11, 9], 10, 7), 5);
      assert.strictEqual(fleet.getNumberOfEngineers([21, 15, 1], 3, 9), 5);
      assert.strictEqual(fleet.getNumberOfEngineers([10], 1, 1), 9);
      assert.strictEqual(fleet.getNumberOfEngineers([10], 2, 3), 3);
      assert.strictEqual(
        fleet.getNumberOfEngineers([8, 9, 10, 14, 15], 9, 5),
        10
      );
    });
  });
});
