/**
 * Finds minimum number of engineers needed to help the FM.
 * @param {Array} scooters - Array of number of scooters per district.
 * @param {Number} C - Number of scooters that the Fleet Manager is able to handle.
 * @param {Number} P - Number of scooters that a Fleet Engineer is able to handle.
 * @return {Number} Minimum number of Fleet Engineers required to help the Fleet Manager.
 */
function getNumberOfEngineers(scooters, C, P) {
  // engineersNeededInManagerDistrict is an array used to store the number of engineers needed
  // in one district (will be used inside the loop when all districts will be iterated).
  // It contains 2 elements. The first one shows the number of engineers needed in the
  // district if the Fleet Manager will NOT be in that district. The second element
  // shows the number of engineers needed if the Fleet Manager will be in that district.
  // Initially the FM district will be the first district.
  const engineersNeededInManagerDistrict = [
    Math.ceil(scooters[0] / P),
    scooters[0] > C ? Math.ceil((scooters[0] - C) / P) : 0
  ];

  // A helper array used for possible new district that the FM can be located in.
  // If this new district presents the opportunity to use less FEs, than this
  // array substitutes the engineersNeededInManagerDistrict bringing a new combination.
  const engineersNeededInManagerDistrictTemp = [];

  // A counter to keep track of engineers needed for all the districts.
  // Recalculated new districts are analysed.
  let totalEngineersNeeded = engineersNeededInManagerDistrict[1];

  // A loop going through each district (starting from the second one since
  // calculations for first districts are already done). If a better combination
  // is spotted than the above variables are recalculated.
  for (let i = 1; i < scooters.length; i += 1) {
    engineersNeededInManagerDistrictTemp[0] = Math.ceil(scooters[i] / P);
    engineersNeededInManagerDistrictTemp[1] =
      scooters[i] > C ? Math.ceil((scooters[i] - C) / P) : 0;

    // Check if the district being analysed will save engineers
    // if the FM were to be located there.
    if (
      engineersNeededInManagerDistrictTemp[0] -
        engineersNeededInManagerDistrictTemp[1] >
      engineersNeededInManagerDistrict[0] - engineersNeededInManagerDistrict[1]
    ) {
      // Fm will be at current district, remake calculations.
      totalEngineersNeeded =
        totalEngineersNeeded -
        engineersNeededInManagerDistrict[1] +
        engineersNeededInManagerDistrict[0] +
        engineersNeededInManagerDistrictTemp[1];

      engineersNeededInManagerDistrict[0] =
        engineersNeededInManagerDistrictTemp[0];
      engineersNeededInManagerDistrict[1] =
        engineersNeededInManagerDistrictTemp[1];
    } else {
      // Fm will continue to be at the district he is at.
      // Add number of engineers needed for this district.
      totalEngineersNeeded += engineersNeededInManagerDistrictTemp[0];
    }
  }

  return totalEngineersNeeded;
}

module.exports = { getNumberOfEngineers };
