# BSGDV Challenge - Node.js
### How to Run

[Node.js](https://nodejs.org/) is required to run this application.

Clone this repo:
```sh
$ git clone https://bitbucket.org/ardibello/bcgdv-node.git
```
Open terminal and navigate to the repository/project root:
```sh
$ cd bcgdv-node
```
Install dependencies:
```sh
$ npm install
```
Run the server:
```sh
$ npm start
```
Make an HTTP POST request to `http://localhost:3000` with a JSON body of the following format:
```json
{
 "scooters": [21, 15, 1],
 "C": 3,
 "P": 9
}
```

### Run Tests
Run the following command in terminal from the project root:
```sh
$ npm test
```