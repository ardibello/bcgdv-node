const express = require('express');
const bodyParser = require('body-parser');
const fleet = require('./fleet');

// Create an HTTP server.
const port = 3000;
const app = express();
app.listen(port, () => console.log(`Server listening on port ${port}!`));

// Parse application/x-www-form-urlencoded and application/json requests.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// POST method route.
app.post('/', (req, res) => {
  const fleetEngineers = fleet.getNumberOfEngineers(
    req.body.scooters,
    req.body.C,
    req.body.P
  );

  res.json({ fleet_engineers: fleetEngineers });
});
